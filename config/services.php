<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'github' => [
        'client_id' => 'your-github-app-id',
        'client_secret' => 'your-github-app-secret',
        'redirect' => 'http://your-callback-url',
    ],
    'google' => [
        'client_id' => '31130147219-asbdakroigj9p6lb1keghebiiqmob9om.apps.googleusercontent.com',
        'client_secret' => '-dJHhqh6IZ0Vs2fsdnR0qiV9',
        'redirect' => 'http://localhost/upsider/public/auth/google/callback',
    ],
    'facebook' => [
        'client_id' => '1778339155791609',
        'client_secret' => '39aa73201988515897802fd99b1d11f4',
        'redirect' => 'http://localhost/upsider/public/auth/facebook/callback',
    ],

];
