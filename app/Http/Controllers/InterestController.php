<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Interest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class InterestController extends Controller
{
    //list All Interest
    public function index()
    {
        $userInterest = Auth::user()->interest;
        $interest = Interest::all();
        if(count($interest) > 0){
            foreach ($interest as $in){
                $in->checked = (is_array($userInterest) && in_array($in->name, $userInterest)) ? true : false;
            }
        }
        return view('interest.subscribe',['interests' => $interest]);
    }

    //update writers interests
    public function subscribe(Request $request)
    {
        $this->validate($request,[
            'interest' => 'required',
        ]);
        $user = Auth::user();
        $user->interest = $request->input('interest');
        $user->save();
        return response()->json(['message' => 'success']);
    }
}
