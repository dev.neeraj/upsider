<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feed;
use Illuminate\Support\Facades\Auth;
use App\Models\Bookmark;
use App\Models\Interest;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tags = Interest::all();
        return view('article.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|min:20|unique:feeds,title|max:200',
            'content' => 'required|min:30',
            'interest_id' => 'required|exists:interests,id'
/*            'featured_image' => 'nullable|active_url'*/
        ]);
        $body = $request->all();
        $body['user_id'] = Auth::user()->id;
        $feed = Feed::create($body);
        return response()->json($feed);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $article)
    {
        return view('article.index',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feed = Feed::where(['user_id' => Auth::user()->id, 'id' => $id])->firstOrFail();
        return view('article.edit',compact('feed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|min:20|max:200',
            'content' => 'required|min:30',
            /*            'featured_image' => 'nullable|active_url'*/
        ]);
        $feed = Feed::where(['user_id' => Auth::user()->id, 'id' => $id])->firstOrFail();
        $feed->update($request->all());
        return response()->json(['message' => 'success']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feed = Feed::where(['user_id' => Auth::user()->id, 'id' => $id])->firstOrFail();
        $feed->delete();
        return response()->json(['message' => 'deleted']);
    }

    //upload Article Media
    public function upload(Request $request)
    {
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $type = $file->getMimeType();
                $size = $file->getClientSize();
                $filename = time() . '-' . $file->getClientOriginalName();
                $file->move(public_path('uploads/post'),$filename);
                $fileUrl = url('/uploads/post/'. $filename);

                $uploadedFiles['files'][] = ['url' => $fileUrl, 'thumbnail_url' => $fileUrl, 'name' => $filename, 'type' => $type, 'size' => $size, 'delete_url' => route('image.destroy',['image' => $filename]), 'delete_type'=> "DELETE" ];
            }

            return $uploadedFiles;
        }else{
            echo 'file not found';
        }
    }

    //Destroy Uploaded Media
    public function destroyUpload(Request $request ,$file)
    {
        $filePath = public_path() . '/uploads/post/'. $file;
        if(file_exists($filePath)) {
            unlink($filePath);
            return response()->json('success');
        } else {
            return response()->json('error',404);
        }
    }

    //Bookmark feed
    public function bookmark(Feed $article)
    {
        Bookmark::firstOrCreate(['user_id' => Auth::id(), 'feed_id' => $article->id]);
        return response()->json(['message' => 'success']);
    }
}
