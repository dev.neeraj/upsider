<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Feed;
use Illuminate\Support\Facades\Auth;
use App\Models\Follower;

class UserController extends Controller
{
    //dashboard View For Viewing Published Post
    public function index()
    {
        $posts = Feed::where(['status' => 'published'])->get();
        return view('board',compact('posts'));
    }

    //view Draft Article
    public function draft()
    {
        $posts = Feed::where(['status' => 'draft', 'user_id' => Auth::user()->id])->get();
        return view('personal.draft',compact('posts'));
    }

    //view Published Article
    public function published()
    {
        $posts = Feed::where(['status' => 'published', 'user_id' => Auth::user()->id])->get();
        return view('personal.draft',compact('posts'));
    }

    //view bookmark Article
    public function bookmarks()
    {
        return view('personal.bookmark')->with('bookmarks',Auth::user()->bookmarks);
    }

    //People To like List
    public function follow()
    {
        $following = Follower::select('follow')->where(['user_id' => Auth::user()->id])->get();
        $users = User::select(['id','name','image'])->where('id','!=',Auth::id());
        if(count($following) > 0){
            $users->where('id','!=',$following->toArray());
        }
        $users = $users->get();
        return view('follow',compact('users'));
    }

    //Follow People You like
    public function startFollowing(Request $request)
    {
        $this->validate($request,['id' => 'required|exists:users,id']);
        Follower::firstOrCreate(['user_id' => Auth::id(), 'follow' => $request->input('id')['id']]);
        return response()->json(['message' => 'success']);
    }
}
