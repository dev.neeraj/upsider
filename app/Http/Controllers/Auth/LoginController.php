<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/board';

    protected $adminRedirect = '/admin/board';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','home']);
    }

    //index function check user role and redirect accordingly.
    public function home()
    {
        if(Auth::check()){
            if(Auth::user()->role == 'writer'){
                return redirect($this->redirectTo);
            }elseif (Auth::user()->role == 'admin'){
                return redirect($this->adminRedirect);
            }
        }
        return view('welcome');
    }

    //Redirect To Google Auth View
    public function redirectToProviderGoogle()
    {
        return Socialite::with('google')->redirect();
    }

    //Handle Callback Functionality and add new user or Login directly
    public function handleProviderCallbackGoogle()
    {
        $user = Socialite::with('google')->user();
        $_user = User::where('email',$user->email)->first();
        if(!$_user){
            $newUser = ['email' => $user->email, 'image' => $user->avatar, 'google_id' => $user->id, 'name' => $user->name];
            $_user = User::create($newUser);
        }
        Auth::login($_user, true);
        return redirect('/board');
    }

    //Redirect To Facebook Auth View
    public function redirectToProviderFacebook()
    {
        return Socialite::with('facebook')->redirect();
    }

    //Handle Callback Functionality and add new user or Login directly
    public function handleProviderCallbackFacebook()
    {
        $user = Socialite::with('facebook')->user();
        $_user = User::where('email',$user->email)->first();
        if(!$_user){
            $newUser = ['email' => $user->email, 'image' => $user->avatar, 'facebook_id' => $user->id, 'name' => $user->name];
            $_user = User::create($newUser);
        }
        Auth::login($_user, true);
        return redirect('/board');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
