<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trending extends Model
{
    public $timestamps = false;

    public $fillable = ['date','feed_id','viewed'];
}
