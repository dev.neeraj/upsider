<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    public $fillable = ['user_id','feed_id'];

    public $timestamps = false;

    public function feed()
    {
        return $this->belongsTo('App\Models\Feed');
    }
}
