<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_id', 'facebook_id','status','image','bio','interest','profession'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['interest' => 'array'];
    
    public function feeds()
    {
        return $this->hasMany('App\Models\Feed');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Models\Bookmark');
    }
}
