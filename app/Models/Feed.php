<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    public $fillable = ['user_id', 'title', 'content', 'category', 'status', 'featured_image','interest_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
