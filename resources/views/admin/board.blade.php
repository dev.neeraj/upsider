@extends('layouts.app')

@section('page-title', 'Upsider - Admin Board')


@section('page-content')
    <div class="container">
        @foreach($posts as $post)
            <div class="row">
                <div class="col-lg-8">
                    <h1><a href="{{ route('article.show', ['article' => $post->id]) }}"> {{ $post->title }}</a></h1>

                    <p class="lead">
                        by <a href="#">{{ $post->user->name }}</a>
                    </p>
                    <hr>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ date('F d, Y', strtotime($post->updated_at)) }}</p>

                    <hr>

                    <!-- Preview Image -->
                    @if($post->featured_image)
                        <img class="img-responsive" src="{{ $post->featured_image }}" alt="">
                    @endif

                    <hr>
                    {!! words($post->content, 100,' ....')  !!}
                    <hr>
                </div> <!-- End row -->
            </div>
        @endforeach

    </div> <!-- container -->

@endsection
