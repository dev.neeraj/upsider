@extends('layouts.app')

@section('page-title', 'People To Follow')

@section('page-css')
    <link href="{{ asset('plugins/toggles/toggles.css') }}" rel="stylesheet">
@endsection


@section('page-content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        @foreach($users as $user)
                            <li class="list-group-item" data-id="{{ $user->id }}">
                                <a href="#">
                                    <div class="avatar">
                                        <img src="{{ $user->image }}" alt="">
                                    </div>
                                    <span class="name">Chadengle</span>
                                </a>
                                <span class="clearfix"></span>
                                <i class="fa fa-user-plus"></i>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div> <!-- col -->
        </div> <!-- End Row -->
    </div> <!-- container -->


@endsection

@section('page-js')

    <script>
        $('ul.contacts-list li').click(function(e){
            e.preventDefault();
            var li = $(this);
            var id = $(this).data(id);
            console.log(id);

            $.ajax({
                type:'post',
                url: '{{ route('user.followstart') }}',
                data: {id:id},
                success: function() {
                    notify('Update Successfully','success');
                    li.remove();
                },
                error: function(response){
                    console.log(response);
                    handleInputErrors(response.responseText);
                }
            });
        });

    </script>

@endsection


