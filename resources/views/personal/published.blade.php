@extends('layouts.master')

@section('page-title', 'Adstair - Users')

@section('page-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
@endsection


@section('page-content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Ad Networks</h4>
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Adstair</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Networks</li>
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Networks</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="m-b-30">
                                    <button  data-toggle="modal" data-target="#addNetworkModal" class="btn btn-primary waves-effect waves-light">Add <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table id="table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($networks as $item)
                                        <tr class="item{{$item->id}}">
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td><button class="edit-modal btn btn-info" data-info="{{$item->id}},{{$item->name}}">
                                                    <span class="glyphicon glyphicon-edit"></span> Edit
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div id="myModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"></h4>

                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" role="form" id="network-form" >
                                                    <input type="hidden" name="id" id="id">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2" for="name">Name</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="name" id="name">
                                                        </div>
                                                    </div>

                                                </form>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn actionBtn">
                                                        <span id="footer_action_button" class='glyphicon'>Save </span>
                                                    </button>
                                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                                        <span class='glyphicon glyphicon-remove'></span> Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="addNetworkModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Add New Network</h4>

                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" role="form" id="network-save" >
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2" for="name">Name</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="name">
                                                        </div>
                                                    </div>

                                                </form>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn actionBtn" id="save_network_button">
                                                        <span  class='glyphicon'>Save </span>
                                                    </button>
                                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                                        <span class='glyphicon glyphicon-remove'></span> Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Row -->
    </div> <!-- container -->


@endsection

@section('page-js')
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>

    <script>

        $(document).on('click', '.edit-modal', function() {
            $('#footer_action_button').text(" Update");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').removeClass('delete');
            $('.actionBtn').addClass('edit');
            $('.modal-title').text('Edit');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            var stuff = $(this).data('info').split(',');
            fillmodalData(stuff)
            $('#myModal').modal('show');
        });

        function fillmodalData(details){
            $('#id').val(details[0]);
            $('#name').val(details[1]);
        }

        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'post',
                url: '{{url('/admin/network/update')}}',
                data: $('#network-form').serialize(),
                success: function(data) {
                    console.log(data);
                    $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" +
                            data.id + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-info='" + data.id+","+data.name+"'><span class='glyphicon glyphicon-edit'></span> Edit</button></tr>");
                    notify('Update Successfully','success');
                    $('#myModal').modal('hide');

                },
                error: function(response){
                    console.log(response);
                    handleInputErrors(response.responseText);
                }
            });
        });

        //save Network
        $('#save_network_button').click(function(){
            $.ajax({
                type:'post',
                url: '{{url('/admin/network/store')}}',
                data: $('#network-save').serialize(),
                success: function() {
                    notify('Update Successfully','success');
                    location.reload();
                },
                error: function(response){
                    console.log(response);
                    handleInputErrors(response.responseText);
                }
            });
        });

    </script>

@endsection


