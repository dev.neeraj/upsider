@extends('layouts.app')

@section('page-title', 'Personal - Draft')

@section('page-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
@endsection


@section('page-content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Draft Post</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table id="table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $item)
                                        <tr class="item{{$item->id}}">
                                            <td>{{ $item->title }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('article.edit', ['article' => $item->id]) }}"> Edit </a>
                                                <button class="btn btn-danger" onclick="deletePost('{{ route('article.destroy', ['article' => $item->id]) }}')"> Delete </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Row -->
    </div> <!-- container -->


@endsection

@section('page-js')
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        } );

        function deletePost(url)
        {
            $.ajax({
                type:'DELETE',
                url: url,
                data: $('#network-save').serialize(),
                success: function() {
                    notify('Delete Successfully','success');
                    location.reload();
                },
                error: function(response){
                    console.log(response);
                    handleInputErrors(response.responseText);
                }
            });
        }
        
    </script>

@endsection


