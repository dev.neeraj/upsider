<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="_token" content="<?php echo csrf_token() ?>"/>
    <link rel="shortcut icon" href="{{asset('images/favicon.jpg')}}">
    <title> @yield('page-title') </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link href="{{ asset('plugins/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/core.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/pages.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css/responsive.css') }}" rel="stylesheet">

    <script src="{{ asset('plugins/js/modernizr.min.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    @yield('page-css')

    {{--Google Analytics--}}
</head>
<body class="fixed-left">


<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="#" class="logo"><i class="md md-terrain"></i> <span> {{ env('APP_NAME') }} </span></a>

            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="fa fa-bars"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect"><i class="md md-crop-free"></i></a>
                        </li>

                        <li class="dropdown">
                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="{{asset('images/user/avatar.png')}}" alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('getprofile') }}"><i class="md md-face-unlock"></i> Profile</a></li>
                                <li><a href="{{ route('getlogout') }}"><i class="md md-settings-power"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-left">
                    <img src="{{asset('images/user/avatar.png')}}" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('getprofile') }}"><i class="md md-face-unlock"></i> Profile<div class="ripple-wrapper"></div></a></li>
                            <li><a href="{{ route('getlogout') }}"><i class="md md-settings-power"></i> Logout</a></li>
                        </ul>
                    </div>

                    <p class="text-muted m-0">{{ Auth::user()->role }}</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                @if(Auth::user()->role == 'writer')
                    <ul>
                        <li>
                            <a href="{{ route('getboard') }}" class="waves-effect waves-light {{ set_active('board') }}"><i class="md md-home"></i><span> Board </span></a>
                        </li>

                        <li>
                            <a href="{{ route('article.create') }}" class="waves-effect waves-light {{ set_active('article/create') }}"><i class="md md-speaker-notes"></i><span> Write </span></a>
                        </li>


                        <li class="has_sub">
                            <a href="#" class="waves-effect waves-light {{ set_active(['personal/bookmark','personal/draft','personal/published']) }}"><i class="md md-pages"></i><span> Personal </span><span class="pull-right"><i class="md md-add"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{ route('personal.bookmark') }}">Bookmark</a></li>
                                <li><a href="{{ route('personal.draft') }}">Draft</a></li>
                                <li><a href="{{ route('personal.published') }}">Published</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('interest.index') }}" class="waves-effect waves-light {{ set_active('interest') }}"><i class="md  md-favorite"></i><span> Manage Interest </span></a>
                        </li>
                        <li>
                            <a href="{{ route('user.follow') }}" class="waves-effect waves-light {{ set_active('follow') }}"><i class="md md-account-child"></i><span> People to Follow </span></a>
                        </li>

                    </ul>
                @elseif(Auth::user()->role == 'admin')
                    <ul>
                        <li>
                            <a href="{{ route('admin.getboard') }}" class="waves-effect waves-light {{ set_active('admin/board') }}"><i class="md md-home"></i><span> Board </span></a>
                        </li>

                        {{--<li>
                            <a href="{{ route('admin.users') }}" class="waves-effect waves-light {{ set_active('admin/users') }}"><i class="md md-home"></i><span> Users </span></a>
                        </li>--}}

                    </ul>
                @endif

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- will be used to show any messages -->
        <div class="content">
            {{--loader--}}
            {{--<div id="target" class="loading">
                <div class="loading-overlay">
                    <p class="loading-spinner">
                        <span class="loading-icon"></span>
                        <span class="loading-text"></span>
                    </p>
                </div>
            </div>--}}
            {{--
            loader ends
            --}}
            @if (session()->has('emsgs'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session('emsgs') !!}
                </div>
            @endif
            @if (session()->has('message'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!! session('message') !!}
                </div>
            @endif

            @yield('page-content')
        </div>

        <footer class="footer text-right">
            2017 © {{ env('APP_NAME') }}.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->



</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>


<!-- Main  -->
<script src="{{ asset('plugins/js/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/js/loading-overlay.min.js') }}"></script>
<script src="{{ asset('plugins/js/noty.min.js') }}"></script>
<script src="{{ asset('plugins/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/js/detect.js') }}"></script>
<script src="{{ asset('plugins/js/fastclick.js') }}"></script>
<script src="{{ asset('plugins/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('plugins/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('plugins/js/waves.js') }}"></script>
<script src="{{ asset('plugins/js/wow.min.js') }}"></script>
<script src="{{ asset('plugins/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('plugins/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('plugins/js/jquery.app.js') }}"></script>
<script src="{{ asset('js/script-master.js') }}"></script>



@yield('page-js')

</body>
</html>

