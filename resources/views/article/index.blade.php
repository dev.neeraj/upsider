@extends('layouts.app')

@section('page-title', $article->title)


@section('page-content')
    <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1><a href="{{ route('article.show', ['article' => $article->id]) }}"> {{ $article->title }}</a></h1>
                    <a href="#" class="pull-right" id="bookmark" data-id="{{ $article->id }}"><i class="fa fa-bookmark"></i></a>
                    <p class="lead">
                        by <a href="#">{{ $article->user->name }}</a>
                    </p>
                    <hr>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ date('F d, Y', strtotime($article->updated_at)) }}</p>

                    <hr>

                    <hr>
                    {!! $article->content !!}
                    <hr>
                </div> <!-- End row -->
            </div>

    </div> <!-- container -->

@endsection

@section('page-js')
    <script type="text/javascript">
        $('#bookmark').click(function (e) {
            e.preventDefault();
            $.ajax({
                type:"GET",
                url:'{{ route('article.bookmark',['article' => $article->id]) }}',
                success:function(){
                    notify('Bookmark Successfully','success');
                },
                error:function(response){
                    handleInputErrors(response.responseText);
                }
            });
        });
    </script>

@endsection