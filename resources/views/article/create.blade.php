@extends('layouts.app')

@section('page-title', 'Upsider - Write A New Article')

@section('page-css')
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('node_modules/medium-editor/dist/css/medium-editor.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/medium-editor/dist/css/themes/default.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/medium-editor-insert-plugin/dist/css/medium-editor-insert-plugin.min.css') }}">
@endsection


@section('page-content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Tags</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="tag" >
                            @forelse($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name}}</option>
                            @empty
                                <option value="">No Tags</option>
                            @endforelse
                        </select>
                    </div>
                </div>

                <div class="pull-right">
                    Featured Image: <input id="fileupload" type="file" name="files[]"  multiple>
                    <img id="uploadPreview" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">New Post</h3></div>
                    <div class="panel-body">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Post Title</label>
                                <div class="col-md-10">
                                    <input type="text" name="title" class="form-control" id="post-title">
                                    <input type="hidden" id="featured_image">
                                </div>
                            </div>
                        <br>
                        <br>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Content</label>
                                <div class="col-md-10">
                                    <div class="editable" id="post-body"></div>
                                    {{--<textarea class="form-control editable" rows="5"></textarea>--}}
                                </div>
                            </div>

                            <div class="form-group m-b-0 ">
                                <div class="col-sm-offset-3 col-sm-9 m-t-40">
                                    <button type="button" class="btn btn-info waves-effect waves-light" onclick="savePost('draft')">Save as Draft</button>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9 m-t-40">
                                    <button type="button" class="btn btn-info waves-effect waves-light" onclick="savePost('published')">Publish Post</button>
                                </div>
                            </div>

                    </div> <!-- panel-body -->
                </div> <!-- panel -->
            </div> <!-- col -->
        </div> <!-- End row -->

        <div id="article-content" style="display: none;">

        </div>

    </div> <!-- container -->

@endsection

@section('page-js')

    <script src="{{ asset('node_modules/medium-editor/dist/js/medium-editor.js') }}"></script>
    <script src="{{ asset('node_modules/handlebars/dist/handlebars.runtime.min.js') }}"></script>
    <script src="{{ asset('node_modules/jquery-sortable/source/js/jquery-sortable-min.js') }}"></script>
    <script src="{{ asset('node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
    <script src="http://linkesch.com/medium-editor-insert-plugin/bower_components/jquery-cycle2/build/jquery.cycle2.min.js"></script>
    <script src="http://linkesch.com/medium-editor-insert-plugin/bower_components/jquery-cycle2/build/plugin/jquery.cycle2.center.min.js"></script>
    <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.js"></script>
    <script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <script src="{{ asset('node_modules/blueimp-file-upload/js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('node_modules/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('node_modules/medium-editor-insert-plugin/dist/js/medium-editor-insert-plugin.min.js') }}"></script>

    <script type="text/javascript">

        var editor = new MediumEditor('.editable');

        $(function () {
            $('.editable').mediumInsert({
                editor: editor,
                addons: { // (object) Addons configuration
                    images: { // (object) Image addon configuration
                        label: '<span class="fa fa-camera"></span>', // (string) A label for an image addon
                        fileDeleteOptions: {}, // (object) extra parameters send on the delete ajax request, see http://api.jquery.com/jquery.ajax/
                        preview: true, // (boolean) Show an image before it is uploaded (only in browsers that support this feature)
                        captions: true, // (boolean) Enable captions
                        autoGrid: 3, // (integer) Min number of images that automatically form a grid
                        formData: {}, // DEPRECATED: Use fileUploadOptions instead
                        fileUploadOptions: { // (object) File upload configuration. See https://github.com/blueimp/jQuery-File-Upload/wiki/Options
                            url: '{{ route('article.upload') }}', // (string) A relative path to an upload script
                            limitMultiFileUploads:2,
                            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i // (regexp) Regexp of accepted file types
                        },
                        styles: { // (object) Available image styles configuration
                            wide: { // (object) Image style configuration. Key is used as a class name added to an image, when the style is selected (.medium-insert-images-wide)
                                label: '<span class="fa fa-align-justify"></span>', // (string) A label for a style
                                added: function ($el) {
                                }, // (function) Callback function called after the style was selected. A parameter $el is a current active paragraph (.medium-insert-active)
                                removed: function ($el) {
                                } // (function) Callback function called after a different style was selected and this one was removed. A parameter $el is a current active paragraph (.medium-insert-active)
                            },
                            left: {
                                label: '<span class="fa fa-align-left"></span>'
                            },
                            right: {
                                label: '<span class="fa fa-align-right"></span>'
                            },
                            grid: {
                                label: '<span class="fa fa-th"></span>'
                            }
                        },
                        actions: { // (object) Actions for an optional second toolbar
                            remove: { // (object) Remove action configuration
                                label: '<span class="fa fa-times"></span>', // (string) Label for an action
                                clicked: function ($el) { // (function) Callback function called when an action is selected
                                    var $event = $.Event('keydown');

                                    $event.which = 8;
                                    $(document).trigger($event);
                                }
                            }
                        },
                        messages: {
                            acceptFileTypesError: 'This file is not in a supported format: ',
                            maxFileSizeError: 'This file is too big: '
                        },
                        uploadCompleted: function ($el, data) {
                            console.log(data);
                        }, // (function) Callback function called when upload is completed
                        uploadFailed: function (uploadErrors, data) {
                            console.log(data);

                        } // (function) Callback function called when upload failed
                    }
                }
            });
        });

        function savePost(status)
        {
            var minLength = 300;
            var category;
            var postContent = editor.serialize();

            $('#article-content').html(postContent['post-body']['value']);
            var numImg = $('#article-content img').length;
            var numIframe = $('#article-content iframe').length;
            var contentLength = $('#article-content').text().length;

            if(contentLength == 0)
            {
                notify('Content Must not Be Empty!','error');
                return false;
            }

            console.log(numImg, contentLength, numIframe);

            if(numIframe > 0){
                category = 'video';
            } else if(contentLength > minLength){
                category = 'story';
            }else{
                category = 'photo';
            }

            var data = {
                title: $('#post-title').val(),
                content: postContent['post-body']['value'],
                category : category,
                featured_image: $('#featured_image').val(),
                status:status,
                interest_id:$('#tag').val()
            };

            console.log(data);

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url : "{{ route('article.store') }}",
                data: data,
                success: function(data) {
                    console.log(data);
                    location.href = '{{ route('getboard')  }}';
                },
                error: function(response) {
                    handleInputErrors(response.responseText);
                }
            });
        }

        /*$('body').on('click', '#save-post', function(e){
            e.preventDefault();


            return false;
        });*/

        //Featured Image Preview.
        $("#fileupload").change(function(){
            readURL(this);
        });

        //upload Featured Image.
        $(function () {
            $('#fileupload').fileupload({
                url: '{{ route('article.upload') }}',
                dataType: 'json',
                done: function (e, data) {
                    notify('Featured Image Uploaded Successfully!','success');
                    console.log(data);
                    $('#featured_image').val(data.result.files[0].url);
                }
            });
        });

    </script>

@endsection