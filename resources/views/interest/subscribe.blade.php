@extends('layouts.app')

@section('page-title', 'Change Interests Subscription')

@section('page-css')
    <link href="{{ asset('plugins/toggles/toggles.css') }}" rel="stylesheet">
@endsection


@section('page-content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Interests</h3></div>
                    <div class="panel-body">

                        <form class="form-vertical" role="form" id="save-form">

                            @foreach($interests as $item)
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">{{ ucfirst($item->name) }}</label>
                                            <div class="col-sm-6 control-label">
                                                <input type="checkbox" {{ $item->checked ? 'checked' : '' }} name="interest[]" value="{{ $item->name }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <button type="button" class="btn actionBtn" id="save-btn"> Save </button>

                        </form>

                    </div> <!-- panel-body -->
                </div> <!-- panel -->
            </div> <!-- col -->
        </div> <!-- End Row -->
    </div> <!-- container -->


@endsection

@section('page-js')

    <script>
        $('#save-btn').click(function(e){
            e.preventDefault();
            console.log($('#save-form').serialize());

            $.ajax({
                type:'post',
                url: '{{ route('interest.subscribe') }}',
                data: $('#save-form').serialize(),
                success: function() {
                    notify('Update Successfully','success');
                    location.reload();
                },
                error: function(response){
                    console.log(response);
                    handleInputErrors(response.responseText);
                }
            });
        });

    </script>

@endsection


