<?php

/*Route::get('/', function () {
    return view('welcome');
})->name('home');*/
Route::get('/', 'Auth\LoginController@home')->name('home');


Route::get('auth/google', 'Auth\LoginController@redirectToProviderGoogle')->name('googleLogin');
Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');

Route::get('auth/facebook', 'Auth\LoginController@redirectToProviderFacebook')->name('facebookLogin');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallbackFacebook');

Route::group(['middleware' => ['web', 'auth']], function(){

    Route::get('logout','Auth\LoginController@logout')->name('getlogout');
    Route::get('profile', 'UserController@profile')->name('getprofile');

});

Route::group(['middleware' => ['web','auth','writer']], function(){
    
    Route::resource('article','ArticleController');
    Route::post('article/upload','ArticleController@upload')->name('article.upload');
    Route::delete('article/{image}','ArticleController@destroyUpload')->name('image.destroy');
    Route::get('article/bookmark/{article}','ArticleController@bookmark')->name('article.bookmark');

    //Personal Routes
    Route::get('personal/draft','UserController@draft')->name('personal.draft');
    Route::get('personal/bookmark','UserController@bookmarks')->name('personal.bookmark');
    Route::get('personal/published','UserController@published')->name('personal.published');

    //Interest Controller
    Route::get('interest','InterestController@index')->name('interest.index');
    Route::post('interest/subscribe','InterestController@subscribe')->name('interest.subscribe');

    //Follow
    Route::get('follow','UserController@follow')->name('user.follow');
    Route::post('startfollowing','UserController@startFollowing')->name('user.followstart');

    Route::get('board','UserController@index')->name('getboard');

});

Route::group(['prefix' => 'admin', 'middleware' => ['web','auth','admin']],function(){

    Route::get('board','UserController@index')->name('admin.getboard');


   /* Route::get('board',function(){
      return view('welcome');
   })->name('admin.getboard');*/

});

