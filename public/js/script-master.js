var debug = true;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    },
    beforeSend: function(){
        $('#target').loadingOverlay();
    },
    success: function(){
        $('#target').loadingOverlay('remove');
    }
});

//log data
function log(data)
{
    if(debug === true){
        console.log(data);
    }
}


/*function handleInputErrors($ele,res){
 $ele.html('');
 $ele.addClass('alert alert-danger alert-block');
 $.each(res.error.message,function(key,value){
 $ele.append(value);
 });
 }*/

function hideAfter($ele){
    setTimeout(function(){
        $ele.hide();
    }, 1500);
}

function emptyAfter($ele){
    setTimeout(function(){
        $ele.removeClass().html('');
    }, 3000);
}

function handleInputErrors(_response)
{
    if(typeof _response == 'string'){
        _response = JSON.parse(_response);
    }
    $.each(_response,function(key,value){
        notify(value,'error');
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploadPreview').attr('src', e.target.result).css("width", "100px").css("height", "100px");
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function notify(msg,$type)
{
    noty({
        text: msg,
        layout: 'bottomRight',
        closeWith: ['click','hover'],
        timeout: 3000,
        type: $type,
    });
}

function ajax(url,type,params,callback,error_handler,complete_callback) {
    params = mergeObjects(params,{ajax:1});
    if(debug) { console.log('Inputs:');console.log(params); }
    $.ajax({
        type: type,
        url : url,
        data: params,
        success: function(response) {
            if(debug) console.log(response);
            if(typeof response.message !== typeof undefined && typeof show_response !== typeof undefined) show_response('success',response.message);
            if(typeof(callback) === 'function') callback(response);
        },
        error: function(error) {
            if(debug) console.log(error.responseText);
            response = JSON.parse(error.responseText);
            if(typeof show_response !== typeof undefined) show_response('error',response.message);
            if(typeof(error_handler) === 'function') error_handler(response,error);
        },
        complete: function(response) {
            if(typeof(complete_callback) === 'function') complete_callback(response);
        }
    });
}

function ajaxSubmit($form,callback,error_handler,reset) {
    $.ajax({
        type: $form.prop('method'),
        url : $form.prop('action'),
        data: $form.serialize(),
        success: function(response) {
            if(debug) console.log(response);
            callback(response);
            if(typeof reset === typeof undefined || reset === true) $form.trigger('reset');
            $('select').trigger('change');
        },
        error: function(error) {
            if(debug) console.log(error.responseText);

            if(error.status === 422) {
                handleInputErrors(JSON.parse(error.responseText));
            } else {
                error_handler(JSON.parse(error.responseText),error);
            }
        }
    });
}

function ajaxUpload($form,options) {
    $form.ajaxSubmit({
        beforeSend: function() {
            var percentVal = '0%';
            options.bar.width(percentVal).attr('data-percentage',percentVal);
            //options.bar_progress.html(percentVal);
            if(typeof options.before === 'function') options.before($form);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            options.bar.width(percentVal).attr('data-percentage',percentVal);
            //options.bar_progress.html(percentVal);
            if(typeof options.progress === 'function') options.progress($form,event,position,total,percentComplete);
        },
        success: function(response) {
            if(debug) console.log(response);
            var percentVal = '100%';
            options.bar.width(percentVal).attr('data-percentage',percentVal);
            //options.bar_progress.html(percentVal);
            if(typeof options.success === 'function') options.success($form,response,options);
        },
        error: function(error){
            if(typeof options.error === 'function') options.error($form,error,options);
        },
        complete: function(xhr) {
            setTimeout(function(){options.bar.closest('.progress').remove();},500);
            if(typeof options.complete === 'function') options.complete($form,xhr,options);
        }
    });
}

/*//Submit ajax function
 function ajax(url, data)
 {
 $.ajax({
 url: "<?php echo site_url('"+ url +"'); ?>",
 type: 'POST',
 data: data,
 dataType : 'json',
 success: function(response)
 {
 console.log(response);
 notify(response.message, 'success');
 },error:function(response){
 console.log(response);
 handleInputErrors(response.responseText);
 }
 });
 }*/

